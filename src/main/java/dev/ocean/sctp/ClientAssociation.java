/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dev.ocean.sctp;

import com.sun.nio.sctp.SctpChannel;
import com.sun.nio.sctp.SctpStandardSocketOptions;
import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.channels.SelectionKey;
import org.apache.log4j.Logger;

/**
 *
 * @author eatakishiyev
 */
public class ClientAssociation extends SctpAssociation {

    private transient final Logger logger = Logger.getLogger(ClientAssociation.class);
    private boolean managementStop = false;
    private boolean autoReconnect = true;
    private boolean sctpNodelay = true;
    private boolean connected = false;

    public ClientAssociation(SctpProvider provider, InetSocketAddress localAddress,
            InetAddress additionalAddress, InetSocketAddress remoteAddress,
            int inStreams, int outStreams) {
        super(provider, localAddress, additionalAddress, remoteAddress, inStreams, outStreams, false);
    }

    public ClientAssociation(SctpProvider provider, InetSocketAddress localAddress,
            InetAddress additionalAddress, InetSocketAddress remoteAddress,
            int inStreams, int outStreams, int rxBufferSize, int txBufferSize) {
        super(provider, localAddress, additionalAddress, remoteAddress, inStreams, outStreams, false, txBufferSize, rxBufferSize);
    }

    @Override
    public void open() throws IOException {
        if (channel == null || !channel.isOpen()) {
            this.managementStop = false;
            this.channel = SctpChannel.open();
            channel.configureBlocking(false);
            channel.bind(getLocalAddress());

            if (getAdditionalAddress() != null) {
                channel.bindAddress(getAdditionalAddress());
            }

            channel.setOption(SctpStandardSocketOptions.SCTP_NODELAY, sctpNodelay);

            channel.connect(getRemoteAddress(), getInStreams(), getOutStreams());
            getProvider().getSelectorThread().register(this, channel, SelectionKey.OP_CONNECT);
        } else if (channel.isOpen()) {
            throw new IOException("Association already connected");
        }
    }

    @Override
    public void accept(SelectionKey key) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void close() throws IOException {
        if (channel == null || (channel != null && !channel.isOpen())) {
            throw new IOException("Association already closed");
        }
        managementStop = true;
        channel.close();
    }

    @Override
    protected void _connect(SelectionKey key) throws Exception {
        while (!channel.finishConnect()) {

        }
        logger.info("Connected " + this);
    }

    @Override
    public boolean isServer() {
        return false;
    }

    @Override
    public void onShutdown() {
        this.connected = false;
        getListener().onCommunicationShutdown(this);
        _reconnect();

    }

    @Override
    public void onCommunicationLost() {
        this.connected = false;
        getListener().onCommunicationDown(this);
        _reconnect();

    }

    @Override
    public void onCommunicationUp() {
        this.connected = true;
        getListener().onCommunicationUp(this);
    }

    protected final void _reconnect() {
        if (!managementStop && autoReconnect) {
            try {
                Thread.sleep(3000);
                channel.close();
            } catch (Exception ex) {
                logger.error("error occured: ", ex);
            }
            channel = null;
            try {
                open();
            } catch (Exception ex) {
                logger.error("error occured: ", ex);
            }
        }

    }

    public boolean isSctpNodelay() {
        return sctpNodelay;
    }

    public void setSctpNodelay(boolean sctpNoDelay) {
        this.sctpNodelay = sctpNoDelay;
    }

    public void setAutoReconnect(boolean autoReconnect) {
        this.autoReconnect = autoReconnect;
    }

    @Override
    public boolean isAutoReconnect() {
        return autoReconnect;
    }

    @Override
    public boolean isServerAssociation() {
        return false;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("ClientAssociation[");
        sb.append("LocalAddress = ").append(getLocalAddress())
                .append(" AdditionalAddress = ").append(getAdditionalAddress())
                .append(" RemoteAddress = ").append(getRemoteAddress())
                .append(" InStreams = ").append(getInStreams())
                .append(" OutStreams = ").append(getOutStreams())
                .append(" AutoReconnect = ").append(autoReconnect)
                .append(" SctpNodelay = ").append(sctpNodelay);
        sb.append("]");
        return sb.toString();
    }

    @Override
    public boolean isConnected() {
        return connected;
    }

}
