/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dev.ocean.sctp;

import com.sun.nio.sctp.MessageInfo;

/**
 *
 * @author eatakishiyev
 */
public interface SctpListener {

    public void onData(byte[] data, MessageInfo msgInfo);

    public void onCommunicationUp(SctpAssociation assoc);

    public void onCommunicationDown(SctpAssociation assoc);

    public void onCommunicationShutdown(SctpAssociation assoc);

    public int getProtocolId();
}
