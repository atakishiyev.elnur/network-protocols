/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dev.ocean.sctp;

import dev.ocean.networking.ConnectionProvider;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;

/**
 *
 * @author eatakishiyev
 */
public class SctpProvider extends ConnectionProvider {

//    private ExecutorService[] executors;
//    private int executorsCount = 5;
    private static SctpProvider instance;

    public static SctpProvider getInstance() throws IOException {
        if (instance == null) {
            synchronized (SctpProvider.class) {
                if (instance == null) {
                    instance = new SctpProvider();
                }
            }
        }
        return instance;
    }

    private SctpProvider() throws IOException {

    }

    public void start() {
//        executors = new ExecutorService[executorsCount];
//        for (int i = 0; i < executorsCount; i++) {
//            this.executors[i] = Executors.newSingleThreadExecutor();
//        }
    }

//    protected ExecutorService getExecutorService(int stream) {
//        int threadIdx = 0;
//        if (stream < executorsCount) {
//            threadIdx = stream;
//        } else {
//            threadIdx = stream / executorsCount;
//        }
//
//        return executors[threadIdx];
//    }
    public ClientAssociation createClientAssociation(InetSocketAddress bindAddress, InetAddress additionalAddress,
            InetSocketAddress remoteAddress, int inStreams, int outStreams) {
        return new ClientAssociation(this, bindAddress, additionalAddress,
                remoteAddress, inStreams, outStreams);
    }

    public ClientAssociation createClientAssociation(InetSocketAddress bindAddress, InetAddress additionalAddress,
            InetSocketAddress remoteAddress, int inStreams, int outStreams, int rxBufferSize, int txBufferSize) {
        return new ClientAssociation(this, bindAddress, additionalAddress, remoteAddress, inStreams, outStreams, rxBufferSize, txBufferSize);
    }

    public ServerAssociation createServerAssociation(InetSocketAddress bindAddress,
            InetAddress additionalAddress, InetSocketAddress remoteAddress,
            int inStreams, int outStreams) {
        return new ServerAssociation(this, bindAddress, additionalAddress,
                remoteAddress, inStreams, outStreams);
    }
}
