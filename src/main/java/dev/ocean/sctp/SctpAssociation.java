/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dev.ocean.sctp;

import com.sun.nio.sctp.AbstractNotificationHandler;
import com.sun.nio.sctp.Association;
import com.sun.nio.sctp.AssociationChangeNotification;
import com.sun.nio.sctp.HandlerResult;
import com.sun.nio.sctp.MessageInfo;
import com.sun.nio.sctp.SctpChannel;
import com.sun.nio.sctp.ShutdownNotification;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.channels.SelectionKey;
import java.util.concurrent.locks.ReentrantLock;

import dev.ocean.networking.Connection;
import org.apache.commons.codec.binary.Hex;
import org.apache.log4j.Logger;

/**
 * @author eatakishiyev
 */
public abstract class SctpAssociation extends Connection {

    private transient final Logger logger = Logger.getLogger(SctpAssociation.class);
    private transient final ReentrantLock writeLock = new ReentrantLock();

    private final int inStreams;
    private final int outStreams;
    private final boolean server;
    private final InetAddress additionalAddress;

    protected transient SctpChannel channel;

    private transient SctpListener listener;

    protected SctpAssociation(SctpProvider provider, InetSocketAddress localAddress,
                              InetAddress additionaAddress, InetSocketAddress remoteAddress,
                              int inStreams, int outStreams, boolean isServer) {
        super(provider, localAddress, remoteAddress);
        this.additionalAddress = additionaAddress;
        this.inStreams = inStreams;
        this.outStreams = outStreams;
        this.server = isServer;
    }

    protected SctpAssociation(SctpProvider provider, InetSocketAddress localAddress,
                              InetAddress additionaAddress, InetSocketAddress remoteAddress,
                              int inStreams, int outStreams, boolean isServer, int txBufferSize, int rxBufferSize) {
        super(provider, localAddress, remoteAddress, txBufferSize, rxBufferSize);

        this.additionalAddress = additionaAddress;
        this.inStreams = inStreams;
        this.outStreams = outStreams;
        this.server = isServer;
    }

    public int getInStreams() {
        return inStreams;
    }

    public int getOutStreams() {
        return outStreams;
    }

    public boolean isServer() {
        return server;
    }

    public void write(byte[] data, int stream, int protocolId) throws IOException {
        writeLock.lock();
        try {
            MessageInfo messageInfo = MessageInfo.createOutgoing(null, stream);
            try {
                txBuffer.clear();
                txBuffer.put(data);
                txBuffer.flip();

                if (logger.isDebugEnabled()) {
                    logger.debug("[SCTP]:Sending data to the peer. "
                            + "Stream = " + stream + " " + this + " Data = "
                            + Hex.encodeHexString(data));
                }
                messageInfo.payloadProtocolID(protocolId);
                long start = System.currentTimeMillis();
                channel.send(txBuffer, messageInfo);
                long end = System.currentTimeMillis();

                if (end - start >= 10) {
                    logger.warn("[SCTPWriteWarning]Delay = " + (end - start));
                }
            } catch (Throwable t) {
                logger.error("[SCTP]:Error while sending message: data = "
                        + Hex.encodeHexString(data) + " StreamId = " + stream + " MesageInfo.stream = " + messageInfo.streamNumber(), t);
            }
        } finally {
            writeLock.unlock();
        }
    }

    @Override
    public void read(SelectionKey key) throws Exception {

        int payloadId = 0;
//        while (true) {
        MessageInfo msgInfo = channel.receive(rxBuffer, this, new AbstractNotificationHandler<Connection>() {
            @Override
            public HandlerResult handleNotification(ShutdownNotification sn, Connection t) {
                logger.warn("[SCTP]:Association shut down received." + t);
                t.onShutdown();
                return super.handleNotification(sn, t); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public HandlerResult handleNotification(AssociationChangeNotification acn, Connection t) {
                switch (acn.event()) {
                    case SHUTDOWN:
                    case COMM_LOST:
                        logger.warn("[SCTP]:Association shut down received." + t);
                        t.onShutdown();
                        break;
                    case COMM_UP:
                        logger.warn("[SCTP]:Association up received." + t);
                        t.onCommunicationUp();
                        break;
                }
                return super.handleNotification(acn, t); //To change body of generated methods, choose Tools | Templates.
            }
        });
        if (msgInfo != null) {
            int readBytes = msgInfo.bytes();
            if (readBytes < 0) {
                logger.warn("[SCTP]:Negative size of read bytes." + this);
                key.cancel();
                channel.close();
                return;
            }

            rxBuffer.flip();
            byte[] data = new byte[msgInfo.bytes()];
            rxBuffer.get(data);

            String hexDump = null;
            if (logger.isDebugEnabled()) {
                hexDump = Hex.encodeHexString(data);
            }

            rxBuffer.clear();

            if (listener != null) {
                if (logger.isDebugEnabled()) {
                    logger.debug(String.format("[SCTP]:Firing incoming data to the"
                                    + "SctpUser. Data = %s. StreamNumber = %s %s ",
                            hexDump, msgInfo.streamNumber(), this));
                }

                listener.onData(data, msgInfo);
            } else {
                logger.warn(String.format("[SCTP]:No listener assignet to the association. Data = %s. StreamNumber = %s %s",
                        hexDump, msgInfo.streamNumber(), this));
            }
        }
    }

    @Override
    public SctpChannel getChannel() {
        return channel;
    }

    public void setListener(SctpListener listener) {
        this.listener = listener;
    }

    public SctpListener getListener() {
        return listener;
    }

    public InetAddress getAdditionalAddress() {
        return additionalAddress;
    }

    public abstract boolean isServerAssociation();

    public boolean isOpen() {
        if (channel == null) {
            return false;
        }

        return channel.isOpen();
    }

    public void shutdown() throws Exception {
        if (channel == null) {
            return;
        }

        channel.shutdown();
    }

    public Association association() throws IOException {
        return channel.association();
    }

    public abstract boolean isAutoReconnect();
}
