/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dev.ocean.sctp;

import com.sun.nio.sctp.SctpServerChannel;
import com.sun.nio.sctp.SctpStandardSocketOptions;
import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.channels.SelectionKey;
import org.apache.log4j.Logger;

/**
 *
 * @author eatakishiyev
 */
public class ServerAssociation extends SctpAssociation {

    private transient final Logger logger = Logger.getLogger(ServerAssociation.class);
    private transient SctpServerChannel serverChannel;
    private boolean connected;

    protected ServerAssociation(SctpProvider provider, InetSocketAddress localAddress,
            InetAddress additionalAddress, InetSocketAddress remoteAddress,
            int inStreams, int outStreams) {
        super(provider, localAddress, additionalAddress, remoteAddress, inStreams,
                outStreams, true);
    }

    @Override
    public void open() throws IOException {
        if (serverChannel == null) {
            serverChannel = SctpServerChannel.open();
            serverChannel.bind(getLocalAddress());
            if (getAdditionalAddress() != null) {
                serverChannel.bindAddress(getAdditionalAddress());
            }
            serverChannel.setOption(SctpStandardSocketOptions.SCTP_INIT_MAXSTREAMS,
                    SctpStandardSocketOptions.InitMaxStreams.create(this.getInStreams(), this.getOutStreams()));
            serverChannel.configureBlocking(false);
            getProvider().getSelectorThread().register(this, serverChannel, SelectionKey.OP_ACCEPT);
            this.connected = true;
        } else if (serverChannel.isOpen()) {
            throw new IOException("Server channel already opened");
        }
    }

    @Override
    public void close() throws IOException {
        if (serverChannel == null
                || (serverChannel != null && !serverChannel.isOpen())) {
            throw new IOException("Server channel already closed");
        } else {
            serverChannel.close();
            serverChannel = null;
            if (channel != null && channel.isOpen()) {
                channel.close();
            }
        }
        this.connected = false;
    }

    @Override
    public boolean isConnected() {
        return connected;
    }
    
    

    @Override
    protected void _connect(SelectionKey key) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void accept(SelectionKey key) throws Exception {
        this.channel = serverChannel.accept();
        this.channel.configureBlocking(false);
        this.channel.setOption(SctpStandardSocketOptions.SCTP_NODELAY, true);
    }

    @Override
    public boolean isServer() {
        return true;
    }

    @Override
    public void onCommunicationLost() {
        getListener().onCommunicationDown(this);
    }

    @Override
    public void onShutdown() {
        getListener().onCommunicationShutdown(this);
    }

    @Override
    public void onCommunicationUp() {
        getListener().onCommunicationUp(this);
    }

    @Override
    public boolean isServerAssociation() {
        return true;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("ServerAssociation[");
        sb.append("LocalAddress = ").append(getLocalAddress())
                .append(" AdditionalAddress = ").append(getAdditionalAddress())
                .append(" RemoteAddress = ").append(getRemoteAddress())
                .append(" InStreams = ").append(getInStreams())
                .append(" OutStreams = ").append(getOutStreams()).append("]");
        return sb.toString();
    }

    @Override
    public boolean isAutoReconnect() {
        return false;
    }

}
