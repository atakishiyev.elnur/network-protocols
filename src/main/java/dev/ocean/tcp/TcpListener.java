/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dev.ocean.tcp;

import dev.ocean.networking.Connection;

import java.net.SocketAddress;

/**
 *
 * @author eatakishiyev
 */
public interface TcpListener {

    public void onData(byte[] data, SocketAddress src);

    public void onCommunicationLost(Connection connection);

    public void onCommunicationUp(Connection connection);
}
