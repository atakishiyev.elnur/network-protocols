/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dev.ocean.tcp;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;

/**
 *
 * @author eatakishiyev
 */
public class ClientConnection extends TcpConnection {

    private boolean connected;

    protected ClientConnection(TcpProvider provider, InetSocketAddress bindAddress, InetSocketAddress remoteAddress) {
        super(provider, bindAddress, remoteAddress, false);
    }

    @Override
    public void open() throws IOException {
        if (channel == null) {
            this.managementStop = false;
            this.channel = SocketChannel.open();
            channel.configureBlocking(false);
            channel.bind(getLocalAddress());
//            channel.bindAddress(new InetSocketAddress("172.18.201.13", 2945).getAddress());

//            channel.setOption(SctpStandardSocketOptions.SCTP_NODELAY, true);
            channel.connect(getRemoteAddress());
            getProvider().getSelectorThread().register(this, channel, SelectionKey.OP_CONNECT);
        } else if (channel.isOpen()) {
            throw new IOException("Association already connected");
        }
    }

    @Override
    public void close() throws IOException {
        if (channel == null || (channel != null && !channel.isOpen())) {
            throw new IOException("Association already closed");
        }
        managementStop = true;
        channel.close();
    }

    @Override
    public void accept(SelectionKey key) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected void _connect(SelectionKey key) throws Exception {
        channel.finishConnect();
    }

    @Override
    public void onCommunicationLost() {
        this.connected = false;
    }

    @Override
    public void onShutdown() {
        this.connected = false;
    }

    @Override
    public void onCommunicationUp() {
        this.connected = true;
    }

    @Override
    public boolean isConnected() {
        return connected;
    }

    
}
