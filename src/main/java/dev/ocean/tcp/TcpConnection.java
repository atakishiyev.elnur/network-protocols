/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dev.ocean.tcp;

import dev.ocean.networking.Connection;
import dev.ocean.networking.ConnectionProvider;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;
import java.nio.channels.spi.AbstractSelectableChannel;
import java.util.concurrent.locks.ReentrantLock;

/**
 *
 * @author eatakishiyev
 */
public abstract class TcpConnection extends Connection {

    private final ReentrantLock writeLock = new ReentrantLock();
    protected boolean managementStop;
    private final boolean server;
    protected SocketChannel channel;

    private TcpListener listener;

    protected TcpConnection(ConnectionProvider provider, InetSocketAddress bindAddress, InetSocketAddress remoteAddress, boolean isServer) {
        super(provider, bindAddress, remoteAddress);
        this.server = isServer;
    }

    @Override
    public void read(SelectionKey key) throws Exception {
        int readBytes = channel.read(rxBuffer);
        if (readBytes < 0) {
            key.cancel();
            channel.close();
            if (listener != null) {
                listener.onCommunicationLost(this);
            }
        } else {
            rxBuffer.flip();
            byte[] data = new byte[rxBuffer.remaining()];
            rxBuffer.get(data);
            if (listener != null) {
                listener.onData(data, ((SocketChannel) key.channel()).getRemoteAddress());
            }
            rxBuffer.clear();
        }
    }

    public void write(byte[] data) throws IOException {
        writeLock.lock();
        try {
            txBuffer.clear();
            txBuffer.put(data);
            txBuffer.flip();

            channel.write(txBuffer);
        } finally {
            writeLock.unlock();
        }
    }

    @Override
    public AbstractSelectableChannel getChannel() {
        return channel;
    }

    public boolean isServer() {
        return server;
    }

    public TcpListener getListener() {
        return listener;
    }

    public void setListener(TcpListener listener) {
        this.listener = listener;
    }
}
