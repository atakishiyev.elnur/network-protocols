/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dev.ocean.tcp;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.SelectionKey;
import java.nio.channels.ServerSocketChannel;

/**
 *
 * @author eatakishiyev
 */
public class ServerConnection extends TcpConnection {

    private ServerSocketChannel serverChannel;
    private boolean connected;

    protected ServerConnection(TcpProvider provider, InetSocketAddress bindAddress, InetSocketAddress remoteAddress) {
        super(provider, bindAddress, remoteAddress, true);
    }

    @Override
    public void open() throws IOException {
        if (serverChannel == null) {
            serverChannel = ServerSocketChannel.open();
            serverChannel.bind(getLocalAddress());
            serverChannel.configureBlocking(false);
            getProvider().getSelectorThread().register(this, serverChannel, SelectionKey.OP_ACCEPT);
            this.connected = true;
        } else if (serverChannel.isOpen()) {
            throw new IOException("Server channel already opened");
        }
    }

    @Override
    public void close() throws IOException {
        if (serverChannel == null
                || (serverChannel != null && !serverChannel.isOpen())) {
            throw new IOException("Server channel already closed");
        } else {
            serverChannel.close();
            serverChannel = null;
            if (channel != null && channel.isOpen()) {
                channel.close();
            }
        }
        this.connected = false;
    }

    @Override
    public void accept(SelectionKey key) throws Exception {
        super.channel = serverChannel.accept();
        super.channel.configureBlocking(false);
    }

    @Override
    protected void _connect(SelectionKey key) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void onCommunicationLost() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void onShutdown() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void onCommunicationUp() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean isConnected() {
        return connected;
    }

}
