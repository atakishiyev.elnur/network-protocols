/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dev.ocean.tcp;

import dev.ocean.networking.ConnectionProvider;

import java.io.IOException;
import java.net.InetSocketAddress;

/**
 *
 * @author eatakishiyev
 */
public class TcpProvider extends ConnectionProvider {

    private static TcpProvider instance;

    public TcpProvider() throws IOException {
    }

    public static TcpProvider getInstance() throws IOException {
        if (instance == null) {
            synchronized (TcpProvider.class) {
                if (instance == null) {
                    instance = new TcpProvider();
                }
            }
        }
        return instance;
    }

    public ClientConnection createClientConnection(InetSocketAddress bindAddress, InetSocketAddress remoteAddress) {
        return new ClientConnection(this, bindAddress, remoteAddress);
    }

    public ServerConnection createServerConnection(InetSocketAddress bindAddress, InetSocketAddress remoteAddress) {
        return new ServerConnection(this, bindAddress, remoteAddress);
    }
}
