/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dev.ocean.networking;

import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 *
 * @author eatakishiyev
 */
public abstract class ConnectionProvider {

    protected SelectorThread selectorThread;
    private final ExecutorService selectorExecutorService = Executors.newSingleThreadExecutor();

    public SelectorThread getSelectorThread() {
        return selectorThread;
    }

    public ConnectionProvider() throws IOException {
        this.selectorThread = new SelectorThread();
        selectorExecutorService.submit(selectorThread);
    }

}
