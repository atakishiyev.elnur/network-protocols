/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dev.ocean.networking;

import java.io.IOException;
import java.io.Serializable;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.spi.AbstractSelectableChannel;

/**
 *
 * @author eatakishiyev
 */
public abstract class Connection implements Serializable{

    protected  final transient ByteBuffer rxBuffer;
    protected final transient ByteBuffer txBuffer;

    private final InetSocketAddress localAddress;
    private final InetSocketAddress remoteAddress;

   transient ConnectionProvider provider;

    protected Connection(ConnectionProvider provider, InetSocketAddress localAddress, InetSocketAddress remoteAddress) {
        this.provider = provider;

        this.localAddress = localAddress;
        this.remoteAddress = remoteAddress;

        this.rxBuffer = ByteBuffer.allocate(4096);
        this.txBuffer = ByteBuffer.allocate(4096);
    }

    protected Connection(ConnectionProvider provider, InetSocketAddress localAddress, InetSocketAddress remoteAddress, int rxBufferSize, int txBufferSize) {
        this.provider = provider;

        this.localAddress = localAddress;
        this.remoteAddress = remoteAddress;

        this.rxBuffer = ByteBuffer.allocate(rxBufferSize);
        this.txBuffer = ByteBuffer.allocate(txBufferSize);

    }

    public abstract void open() throws IOException;

    public abstract void close() throws IOException;

    public abstract void read(SelectionKey key) throws Exception;

    public abstract void accept(SelectionKey key) throws Exception;

    protected abstract void _connect(SelectionKey key) throws Exception;

    public abstract void onCommunicationLost();

    public abstract void onShutdown();
    
    public abstract boolean isConnected();

    public abstract void onCommunicationUp();

    public InetSocketAddress getRemoteAddress() {
        return remoteAddress;
    }

    public InetSocketAddress getLocalAddress() {
        return localAddress;
    }

    protected ConnectionProvider getProvider() {
        return this.provider;
    }

    public abstract AbstractSelectableChannel getChannel();

}
