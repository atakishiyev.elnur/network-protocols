/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dev.ocean.networking;

import java.io.IOException;
import java.nio.channels.ClosedChannelException;
import java.nio.channels.SelectableChannel;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.util.Iterator;
import org.apache.log4j.Logger;

/**
 *
 * @author eatakishiyev
 */
public class SelectorThread implements Runnable {

    private final Logger logger = Logger.getLogger(SelectorThread.class);
    private final Selector selector;

    public SelectorThread() throws IOException {
        this.selector = Selector.open();
    }

    public void register(Connection association, SelectableChannel channel, int interesting) throws ClosedChannelException {
        channel.register(selector, interesting, association);
        selector.wakeup();
    }

    @Override
    public void run() {
        while (true) {
            try {
                selector.select(5);

                Iterator iter = selector.selectedKeys().iterator();
                while (iter.hasNext()) {
                    SelectionKey key = (SelectionKey) iter.next();
                    iter.remove();
                    if (key.isValid()) {
                        if (key.isAcceptable()) {
                            Connection connection = (Connection) key.attachment();
                            connection.accept(key);
                            connection.getChannel().register(selector, SelectionKey.OP_READ, connection);
                        }

                        if (key.isReadable()) {
                            Connection connection = (Connection) key.attachment();
                            connection.read(key);
                        }

                        if (key.isValid() && key.isConnectable()) {
                            Connection connection = (Connection) key.attachment();
                            try {
                                connection._connect(key);
                                connection.getChannel().register(selector, SelectionKey.OP_READ, connection);

                            } catch (Exception ex) {
                                logger.error("error occured while finish connection: ", ex);
                                connection.onCommunicationLost();
                            }
                        }
                    }

                }
            } catch (Exception ex) {
                logger.error(":[SelectorThread]:Someting gone wrong during select on selector", ex);
            }
        }
    }

}
