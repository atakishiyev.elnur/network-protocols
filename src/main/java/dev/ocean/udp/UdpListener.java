/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dev.ocean.udp;

import java.net.SocketAddress;

/**
 *
 * @author eatakishiyev
 */
public interface UdpListener {

    public void onData(byte[] data, SocketAddress src);
    
    public void onException(Exception exception);
}
