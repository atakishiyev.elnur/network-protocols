/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dev.ocean.udp;

import dev.ocean.networking.ConnectionProvider;

import java.io.IOException;
import java.net.InetSocketAddress;

/**
 *
 * @author eatakishiyev
 */
public class UdpProvider extends ConnectionProvider {

    private static UdpProvider instance;

    private UdpProvider() throws IOException {

    }

    public static UdpProvider getInstance() throws IOException {
        if (instance == null) {
            synchronized (UdpProvider.class) {
                if (instance == null) {
                    instance = new UdpProvider();
                }
            }
        }

        return instance;
    }

    public UdpConnection createConnection(InetSocketAddress localAddress, InetSocketAddress remoteAddress) {
        return new UdpConnection(this, localAddress, remoteAddress);
    }
}
