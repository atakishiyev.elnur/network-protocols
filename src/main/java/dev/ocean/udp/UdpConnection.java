/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dev.ocean.udp;

import dev.ocean.networking.Connection;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.channels.DatagramChannel;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;
import java.nio.channels.spi.AbstractSelectableChannel;
import java.util.concurrent.locks.ReentrantLock;

/**
 *
 * @author eatakishiyev
 */
public class UdpConnection extends Connection {

    protected DatagramChannel udpChannel;
    private UdpListener listener;

    private final ReentrantLock mutex = new ReentrantLock();

    protected UdpConnection(UdpProvider provider, InetSocketAddress localAddress,
            InetSocketAddress remoteAddress) {
        super(provider, localAddress, remoteAddress);
    }

    @Override
    public void read(SelectionKey key) throws Exception {
        int readBytes = 0;
        try {
            readBytes = udpChannel.read(rxBuffer);
        } catch (Exception ex) {
            if (listener != null) {
                listener.onException(ex);
            }
        }
        if (readBytes > 0) {
            rxBuffer.flip();
            byte[] data = new byte[rxBuffer.remaining()];
            rxBuffer.get(data);
            if (listener != null) {
                listener.onData(data, ((DatagramChannel) key.channel()).
                        getRemoteAddress());
            }
            rxBuffer.clear();
        }
    }

    public void send(byte[] data) throws IOException {
        mutex.lock();
        try {
            txBuffer.clear();
            txBuffer.put(data);
            txBuffer.flip();
            udpChannel.send(txBuffer, getRemoteAddress());
        } finally {
            mutex.unlock();

        }
    }

    @Override
    public AbstractSelectableChannel getChannel() {
        return udpChannel;
    }

    public UdpListener getListener() {
        return listener;
    }

    public void setListener(UdpListener listener) {
        this.listener = listener;
    }

    @Override
    public void open() throws IOException {
        if (udpChannel == null) {
            udpChannel = DatagramChannel.open();
            udpChannel.configureBlocking(false);
            udpChannel.bind(getLocalAddress());
            udpChannel.connect(getRemoteAddress());

            getProvider().getSelectorThread().register(this, udpChannel, SelectionKey.OP_READ);
        }
    }

    @Override
    public void onCommunicationLost() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void onCommunicationUp() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void onShutdown() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected void _connect(SelectionKey key) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void accept(SelectionKey key) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void close() throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean isConnected() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
